﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ghost : MonoBehaviour {
    GameManager _gameManager;
    NavMeshAgent _navAgent;
    GameObject _player;
    PacManMovement _pacman;
    [SerializeField]
    GameObject GhostMesh;
    [SerializeField]
    GameObject EyeMesh;
    [SerializeField]
    Transform SpawnPoint;
    bool _isDead;
    static public int points =200;
    Collider _collider;
    [SerializeField]
    Material _afraidMaterial;
    Material _currentMaterial;
    Renderer _ghostMeshRenderer;
    [SerializeField]
    Material _standardMaterial;
    private void Awake()
    {
        _gameManager = GameObject.FindObjectOfType<GameManager>();
        _collider = gameObject.GetComponent<Collider>();
        _navAgent=GetComponent<NavMeshAgent>();
        _currentMaterial = GetComponent<Renderer>().material;
        _currentMaterial = GhostMesh.transform.GetComponent<Renderer>().material;
        _ghostMeshRenderer = GhostMesh.GetComponent<Renderer>();
        _standardMaterial = _ghostMeshRenderer.material;
    }
    // Use this for initialization
    void OnDisable()
    {
        Destroy(GhostMesh);
    }
    void Start () {
        _player = GameObject.FindGameObjectWithTag("Player");
        if (_player == null)
            throw new UnityException("player is missing");
        if (SpawnPoint == null)
            throw new UnityException("Spawn point is missing");
        _pacman = _player.GetComponent<PacManMovement>();
        //  _navAgent.SetDestination(_player.transform.position);
        
        EyeMesh.SetActive(false);

	}
	
    

	// Update is called once per frame
	void Update () {
        if (_gameManager.IsGameReady() == false)
            return;
        //_player = GameObject.FindGameObjectWithTag("Player");
        GhostMesh.transform.position = transform.position;
        EyeMesh.transform.position = transform.position;
        if (_isDead == false)
        {
            _navAgent.SetDestination(_player.transform.position);
            SetMaterial();
        }


        else
        {
            if (IsInsideSpawnZone())
            {
                Respawn();

            }
        }
    }

    private void Respawn()
    {
        Debug.Log("respawn");
        GhostMesh.SetActive(true);
        EyeMesh.SetActive(false);
        _isDead = false;
        _collider.enabled = true;
    }

    private void SetMaterial()
    {
        if (_pacman.HasPowerUp())
            _ghostMeshRenderer.material = _afraidMaterial;
        else
            _ghostMeshRenderer.material = _standardMaterial;
    }

    bool IsInsideSpawnZone()
    {
        return (gameObject.transform.position - SpawnPoint.transform.position).magnitude <= 2;
    }
    public void OnDeath()
    {
        _collider.enabled = false;
        _isDead = true;
        GhostMesh.SetActive(false);
        EyeMesh.SetActive(true);
        _navAgent.SetDestination(SpawnPoint.transform.position);

    }
}
