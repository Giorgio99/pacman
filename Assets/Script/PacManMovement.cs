﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacManMovement : MonoBehaviour
{

    [SerializeField]
    float MovementSpeed;

    int _totalPoints;
    public int GetTotalPoints()
    {
        return _totalPoints;
    }
    int Lives = 3;
    public int GetLives()
    {
        return Lives;
    }
    GameManager _gameManager;
    AudioSource _audioSource;

    [SerializeField]
    AudioClip _audioMov;

    [SerializeField]
    AudioClip _audioDeath;
    
    /// <summary>
    /// Ha o meno il power up
    /// </summary>
    bool _hasPowerUp;
    public bool HasPowerUp()
    {
        return _hasPowerUp;
    }
    /// <summary>
    /// Tempo trascorso da quando si è raccolto il power up
    /// </summary>
    float _powerUpElapsedTime = 0;
    /// <summary>
    /// Indica la durata dell'ultimo power up raccolto
    /// </summary>
    float _powerUpDuration = 3;
    /// <summary>
    /// Numero di fantasmi mangiati durante il power up
    /// </summary>
    int _eatenGhosts;
    /// <summary>
    /// Numero di pillole mangiate
    /// </summary>
    [SerializeField]
    int _eatenPills;
    public int GetEatenPills()
    {
        return _eatenPills;
    }
    /// <summary>
    /// variabile per decidere quando far iniziare a muovere i personaggi e subito dopo metodo per dare visibilità globale
    /// senza rendere pubblica la variabile questa tecnica è detta incapsulamento (in genere il metodo si chiama get+nome)
    /// e fa parte dei metodi accessori untili in progetti medio-grandi per evitare di trovarsi con troppe variabili 
    /// pubbliche e fare un casino è anche molto comune creare variabili accessorie per cambiare il valore di una variabile
    /// pubblica (in genere prendono il nome di set+nome)
    /// </summary>
    

    private void Awake()
    {
        _gameManager = GameObject.FindObjectOfType<GameManager>();
        _audioSource = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start()
    {
        Debug.Log("Totale pillole " + pill.GetTotalPillCount());
    }
   

    // Update is called once per frame
    void Update()
    {
        if (_gameManager.IsGameReady() == false)
            return;
        float h = Input.GetAxis("Horizontal");
       
        float v = Input.GetAxis("Vertical");
        

        if (h != 0)
        {
            transform.Translate(Vector3.right * MovementSpeed * h);

        }
        else
        {
            transform.Translate(Vector3.forward * MovementSpeed * v);
        }

        if (h != 0 || v != 0)
        {
            if (!_audioSource.isPlaying)
            {
                //_audioSource.Stop();
                _audioSource.PlayOneShot(_audioMov);
            }
        }
        if (_hasPowerUp)
        {
            _powerUpElapsedTime += Time.deltaTime;
            //Debug.Log("Elapsed time " + _powerUpElapsedTime);
            if (_powerUpElapsedTime >= _powerUpDuration)
            {
                _hasPowerUp = false;
                _eatenGhosts = 0;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("GNAM!");

        if (other.gameObject.tag == "pill" || other.gameObject.tag == "powerup")
        {
            OnEatPill(other);
        }

        if (other.gameObject.tag == "ghost")
        {
            if (!_hasPowerUp)
                OnHit();
            else
            {
                GhostHit(other);

            }
        }
       
    }

    private void GhostHit(Collider other)
    {
        if (other == null)
            throw new System.ArgumentNullException("other", "other cannot be null");
        _eatenGhosts++;
        _totalPoints += (int)Mathf.Pow(2, _eatenGhosts - 1) * Ghost.points;
        //Destroy(other.gameObject);
        other.gameObject.SendMessage("OnDeath", SendMessageOptions.RequireReceiver);
    }

    //Codice eseguito quando si mangia la pillola
    void OnEatPill(Collider other)
    {
        pill pill = other.gameObject.GetComponent<pill>();
        _eatenPills++;
        //questa riga serve ad andare a predere le proprietà di pill, di ogni singola sfera, e ci permette di usarla.
        // pill pill = other.gameObject.GetCmponent(typeof(pill)); possibile alternativa
        _totalPoints += pill.points;
        if (pill is PowerUp)
        {
            Debug.Log("Power up!");
            _hasPowerUp = true;
            _powerUpElapsedTime = 0;
        }
        Destroy(other.gameObject);
        
    }

    void OnHit()
    {
        Debug.Log("GAME OVER");
        if (!_audioSource.isPlaying)
            _audioSource.PlayOneShot(_audioDeath);

        Lives -= 1;
    }
}
