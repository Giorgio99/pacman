﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScore : MonoBehaviour {


    [SerializeField]
    PacManMovement pacman;
    [SerializeField]
    Text _text;

    private void Awake()
    {
        _text = GetComponent<Text>();
        if (_text == null)
        {
            string message = "text component is missing";
            Debug.LogError(message);
            throw new UnityException(message);
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        _text.text = pacman.GetTotalPoints().ToString();
	}
}
