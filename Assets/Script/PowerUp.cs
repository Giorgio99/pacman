﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : pill {

    
    bool working = false;
    
    // Use this for initialization
    void Start()
    {
        StartCoroutine(Animate());
    }

    IEnumerator Animate()
    {
        working = true;
        float waitTime = 0.1f;
        while (working == true)
        {
            transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            yield return new WaitForSeconds(waitTime);
            //dopo questo l'esecuzione del programma si arresta per 0.5 secondi
            transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            yield return new WaitForSeconds(waitTime);
            //Debug.Log("Animation in progress");
        }
    }
   
    // Update is called once per frame
    void Update()
    {

    }

   
}
