﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    AudioSource _audioSource;
    [SerializeField]
    PacManMovement _pacMan;
    [SerializeField]
    GameObject _gameOverUI;
    [SerializeField]
    GameObject _restartLevelButton;
    [SerializeField]
    AudioClip _audioStart;
    bool _isGameReady = false;
    public bool IsGameReady()
    {
        return _isGameReady;
    }

    // Use this for initialization
    void Start () {
        if (_pacMan == null)
            throw new UnityException("Pacman is missing");
        _gameOverUI.SetActive(false);
        _restartLevelButton.SetActive(false);
        StartCoroutine(CheckStartMusic());
    }
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update () {
        
        if (CheckDefeat())
        {
            OnDefeat();
        }
        else if (CheckVictory())
        {
            OnVictory();
        }
	}

    private void OnVictory()
    {
        Debug.Log("Hai vinto");
        SceneManager.LoadScene("stage02");
    }
    private void OnDefeat()
    {
        _gameOverUI.SetActive(true);
        _restartLevelButton.SetActive(true);
        _isGameReady = false;
    }
    /// <summary>
    /// Cntrolla la condizione di vittoria
    /// </summary>
    /// <returns>true se vince altrimenti false</returns>
    bool CheckVictory()
    {
        return (_pacMan.GetEatenPills() == pill.GetTotalPillCount());
       
    }
    bool CheckDefeat()
    {
        return (_pacMan.GetLives()==0);
    }
    /// <summary>
    /// Aspetta che la musica di inizio sia terminata prima di lasciar partire i personaggi
    /// </summary>
    IEnumerator CheckStartMusic()
    {
        _isGameReady = false;
        _audioSource.PlayOneShot(_audioStart);
        while (_audioSource.isPlaying)
            yield return null;
        _isGameReady = true;
    }
    public void RestartLevel()
    {
        SceneManager.LoadScene("stage01");
    }
}
